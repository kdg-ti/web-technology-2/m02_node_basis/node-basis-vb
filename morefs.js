/** Created by Jan de Rijke */
/* Deze code verwijdert de bestanden die aangemaakt zijn met filesystemAsync.js
Run dus steeds  filesystemAsync.js vóór deze script
 */

import * as  fs from "node:fs";
import * as  path from "node:path";
console.log(process.cwd());// working directory. Alle relatieve pathnames zijn tov cwd
process.chdir('mapje');
fs.readdir('.',(err,ls) => console.log(ls)); // ls is an array of filenames
process.chdir('..');
fs.unlink(path.join('mapje','koppie.txt'), err => fs.rmdir('mapje',
	err=> {if(err){console.log(err)}}
	)
);
