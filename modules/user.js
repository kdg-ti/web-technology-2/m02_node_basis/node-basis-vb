/** Created by Jan de Rijke */
import {PI} from "./calc.js"
import {product, substract as min} from "./calc.js"
import sum from "./calc.js"
import * as reken from  "./calc.js"

console.log("Surface of circle with radius 5 is " + product(PI,product(5,5)).toFixed(2) );
console.log(min(10,PI).toFixed(2));
console.log(sum(10,1));
console.log (reken.substract(4,5))
